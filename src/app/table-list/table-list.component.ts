import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';

import { DashboardServiceService } from '../services/dashboard-service.service';

class ListModel {
  dateUpload: string;
  docType: string;
  filePath: string;
  id: number;
  status: string;
  uploader: string;
}


@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  docType: string;
  list: ListModel[] = [];
  dtTrigger = new Subject();

  constructor(
    private dashboardService: DashboardServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers'
    };

    this.route.params.subscribe((params) => {
      this.docType = params.docType;
      this.getDocList();
    });
  }

  getDocList() {
    this.dashboardService.getList(this.docType).subscribe((data: ListModel[]) => {
      this.list = data;
      this.dtTrigger.next();
    });
  }

  navigateToDetails(id) {
    this.router.navigateByUrl(`/details/${this.docType}/${id}`);
  }
}
