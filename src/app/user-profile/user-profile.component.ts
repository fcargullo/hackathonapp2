import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DashboardServiceService } from '../services/dashboard-service.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  docType: string;
  id: string;
  pdf: string;

  constructor(
    private dashboardService: DashboardServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.docType = params.docType;
      this.id = params.id;
    });

    this.dashboardService.getDetails().subscribe((data) => {
      console.log(data);
    });
  }

}
