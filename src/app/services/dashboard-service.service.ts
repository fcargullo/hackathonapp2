import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardServiceService {

  docUrl = 'http://10.242.102.84:8080/document';
  constructor(
    private http: HttpClient
  ) { }

  getList(param: string) {
    const url = this.getUrl(param);
    return this.http.get(url);
  }

  getUrl(endpoint: string): string {
    let url;
    switch (endpoint) {
      case 'claims':
        url = `${this.docUrl}/claims`;
        return url;
      case 'application':
        url = `${this.docUrl}/application`;
        return url;
      case 'survey':
        url = `${this.docUrl}/survey`;
        return url;
      default:
        url = `${this.docUrl}/getAllDocument`;
        return url;
    }
  }

  getCount() {
    const url = `${this.docUrl}/dashboard`;
    return this.http.get(url);
  }

  // getDetails(docType, id) {
  getDetails() {
    const url = `${this.docUrl}/document/testPDF`;
    return this.http.get(url);
  }
}
